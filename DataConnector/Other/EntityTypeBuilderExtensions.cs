﻿using System.Linq.Expressions;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DataConnector.Other;

public static class EntityTypeBuilderExtensions
{
	public static EntityTypeBuilder<TEntity> MapProperty<TEntity, TProperty>(
		this EntityTypeBuilder<TEntity> builder,
		Expression<Func<TEntity, TProperty>> propertyExpression,
		string columnName
	) where TEntity : class
	{
		builder
			.Property(propertyExpression)
			.HasColumnName(columnName);
		return builder;
	}

	public static EntityTypeBuilder<TEntity> ManyToOne<TEntity, TRelatedEntity>(
		this EntityTypeBuilder<TEntity> builder,
		Expression<Func<TEntity, IEnumerable<TRelatedEntity>?>>? many,
		Expression<Func<TRelatedEntity, TEntity?>>? one,
		Expression<Func<TRelatedEntity, object?>> foreignKey
	)
		where TEntity : class
		where TRelatedEntity : class
	{
		builder
			.HasMany(many)
			.WithOne(one)
			.HasForeignKey(foreignKey);

		return builder;
	}

	public static EntityTypeBuilder<TEntity> OneToManyAnonymous<TEntity, TRelatedEntity>(
		this EntityTypeBuilder<TEntity> builder,
		Expression<Func<TEntity, TRelatedEntity?>>? one,
		Expression<Func<TEntity, object?>> foreignKey
	)
		where TEntity : class
		where TRelatedEntity : class
	{
		builder
			.HasOne(one)
			.WithMany()
			.HasForeignKey(foreignKey);

		return builder;
	}

	public static EntityTypeBuilder<TEntity> OneToOneAnonymous<TEntity, TRelatedEntity>(
		this EntityTypeBuilder<TEntity> builder,
		Expression<Func<TEntity, TRelatedEntity?>>? one,
		Expression<Func<TEntity, object?>> foreignKey
	)
		where TEntity : class
		where TRelatedEntity : class
	{
		builder
			.HasOne(one)
			.WithOne()
			.HasForeignKey(foreignKey);

		return builder;
	}

	public static EntityTypeBuilder<TEntity> OneToOne<TEntity, TRelatedEntity>(
		this EntityTypeBuilder<TEntity> builder,
		Expression<Func<TEntity, TRelatedEntity?>>? one,
		Expression<Func<TRelatedEntity, TEntity?>>? anotherOne,
		Expression<Func<TRelatedEntity, object?>> foreignKey
	)
		where TEntity : class
		where TRelatedEntity : class
	{
		builder
			.HasOne(one)
			.WithOne(anotherOne)
			.HasForeignKey(foreignKey);

		return builder;
	}

	public static EntityTypeBuilder<TEntity> OwnsSingle<TEntity, TRelatedEntity>(
		this EntityTypeBuilder<TEntity> builder,
		Expression<Func<TEntity, TRelatedEntity?>>? many,
		Expression<Func<TRelatedEntity, TEntity?>>? one,
		Expression<Func<TRelatedEntity, object?>> foreignKey
	)
		where TEntity : class
		where TRelatedEntity : class
	{
		builder
			.HasOne(many)
			.WithOne(one)
			.HasForeignKey(foreignKey);

		return builder;
	}

	public static EntityTypeBuilder<TEntity> ValueGeneratedOnAdd<TEntity, TProperty>(
		this EntityTypeBuilder<TEntity> builder,
		Expression<Func<TEntity, TProperty>> propertyExpression
	)
		where TEntity : class
	{
		builder
			.Property(propertyExpression)
			.ValueGeneratedOnAdd();
		return builder;
	}

	public static EntityTypeBuilder<TEntity> ValueGeneratedOnAddOrUpdate<TEntity, TProperty>(
		this EntityTypeBuilder<TEntity> builder,
		Expression<Func<TEntity, TProperty>> propertyExpression
	)
		where TEntity : class
	{
		builder
			.Property(propertyExpression)
			.ValueGeneratedOnAddOrUpdate();
		return builder;
	}

	public static EntityTypeBuilder<TEntity> ValueGeneratedOnAdd<TEntity, TProperty>(
		this EntityTypeBuilder<TEntity> builder,
		Expression<Func<TEntity, TProperty>> propertyExpression,
		TProperty defaultValue
	)
		where TEntity : class
	{
		builder
			.Property(propertyExpression)
			.HasDefaultValue(defaultValue)
			.ValueGeneratedOnAdd();
		return builder;
	}

	public static EntityTypeBuilder<TEntity> ValueGeneratedOnAddOrUpdate<TEntity, TProperty>(
		this EntityTypeBuilder<TEntity> builder,
		Expression<Func<TEntity, TProperty>> propertyExpression,
		TProperty defaultValue
	)
		where TEntity : class
	{
		builder
			.Property(propertyExpression)
			.HasDefaultValue(defaultValue)
			.ValueGeneratedOnAddOrUpdate();
		return builder;
	}
}