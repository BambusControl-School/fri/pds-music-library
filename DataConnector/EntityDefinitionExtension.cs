﻿using DataConnector.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DataConnector;

internal static class EntityDefinitionExtension
{
	internal static EntityTypeBuilder<Zaner> Define(this EntityTypeBuilder<Zaner> entity)
	{
		entity
			.ToTable("H_ZANRE")
			.HasKey(zaner => zaner.Id);

		return entity;
	}

	internal static EntityTypeBuilder<Krajina> Define(this EntityTypeBuilder<Krajina> entity)
	{
		entity
			.ToTable("H_KRAJINY")
			.HasKey(krajina => krajina.Id);

		return entity;
	}

	internal static EntityTypeBuilder<Interpret> Define(this EntityTypeBuilder<Interpret> entity)
	{
		entity
			.ToTable("H_INTERPRETI")
			.HasKey(interpret => interpret.Id);

		entity
			.HasOne(interpet => interpet.Zaner)
			.WithMany(zaner => zaner.Interpreti)
			.HasForeignKey(interpret => interpret.IdZaner);

		entity
			.HasOne(interpet => interpet.Krajina)
			.WithMany(zaner => zaner.Interpreti)
			.HasForeignKey(interpret => interpret.IdKrajina);

		return entity;
	}

	internal static EntityTypeBuilder<Vydavatelstvo> Define(this EntityTypeBuilder<Vydavatelstvo> entity)
	{
		entity
			.ToTable("H_VYDAVATELSTVA")
			.HasKey(vydavatelstvo => vydavatelstvo.Id);

		entity
			.HasOne(vydavatelstvo => vydavatelstvo.Krajina)
			.WithMany(krajina => krajina.Vydavatelstva)
			.HasForeignKey(vydavatelstvo => vydavatelstvo.IdKrajina);

		return entity;
	}

	internal static EntityTypeBuilder<Album> Define(this EntityTypeBuilder<Album> entity)
	{
		entity
			.ToTable("H_ALBUMY")
			.HasKey(album => album.Id);

		entity
			.HasOne(album => album.Vydavatel)
			.WithMany(vydavatelstvo => vydavatelstvo.Albumy)
			.HasForeignKey(album => album.IdVydavatel);

		entity
			.HasOne(album => album.Zaner)
			.WithMany(zaner => zaner.Albumy)
			.HasForeignKey(album => album.IdZaner);

		entity
			.HasOne(album => album.HlavnyInterpret)
			.WithMany(interpret => interpret.Albumy)
			.HasForeignKey(album => album.IdHlavnyInterpret);

		return entity;
	}

	internal static EntityTypeBuilder<RelationInterpretSkladby> Define(
		this EntityTypeBuilder<RelationInterpretSkladby> relationEntity
	)
	{
		relationEntity
			.ToTable("H_INTERPRETI_SKLADBY")
			.HasKey(e => new {e.IdSkladba, e.IdInterpret});

		relationEntity
			.HasOne<Skladba>()
			.WithMany()
			.HasForeignKey(e => e.IdSkladba);

		relationEntity
			.HasOne<Interpret>()
			.WithMany()
			.HasForeignKey(e => e.IdInterpret);

		return relationEntity;
	}

	internal static EntityTypeBuilder<Skladba> Define(this EntityTypeBuilder<Skladba> entity)
	{
		entity
			.ToTable("H_SKLADBY")
			.HasKey(skladba => skladba.Id);

		entity
			.HasOne(skladba => skladba.Zaner)
			.WithMany(zaner => zaner.Skladby)
			.HasForeignKey(skladba => skladba.IdZaner);

		entity
			.HasOne(skladba => skladba.Album)
			.WithMany(album => album.Skladby)
			.HasForeignKey(skladba => skladba.IdAlbum);

		entity
			.HasMany(skladba => skladba.Interpreti)
			.WithMany(interpret => interpret.Skladby)
			.UsingEntity<RelationInterpretSkladby>();

		return entity;
	}

	internal static EntityTypeBuilder<RelationPlaylistSkladby> Define(
		this EntityTypeBuilder<RelationPlaylistSkladby> relationEntity
	)
	{
		relationEntity
			.ToTable("H_SKLADBY_PLAYLISTU")
			.HasKey(e => new {e.IdPlaylist, e.IdSkladba});

		relationEntity.HasOne<Playlist>()
			.WithMany()
			.HasForeignKey(e => e.IdPlaylist);

		relationEntity
			.HasOne<Skladba>()
			.WithMany()
			.HasForeignKey(e => e.IdSkladba);

		return relationEntity;
	}

	internal static EntityTypeBuilder<Playlist> Define(this EntityTypeBuilder<Playlist> entity)
	{
		entity
			.ToTable("H_PLAYLISTY")
			.HasKey(playlist => playlist.Id);

		entity
			.HasMany(playlist => playlist.Skladby)
			.WithMany(skladba => skladba.Playlisty)
			.UsingEntity<RelationPlaylistSkladby>();

		return entity;
	}
}
