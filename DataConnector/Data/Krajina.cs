﻿using System.Diagnostics.CodeAnalysis;

namespace DataConnector.Data;

[SuppressMessage("ReSharper", "ClassNeverInstantiated.Global")]
[SuppressMessage("ReSharper", "MemberCanBeInternal")]
[SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Global")]
[SuppressMessage("ReSharper", "CollectionNeverUpdated.Global")]
public class Krajina
{
	public int Id { get; set; }
	public string? Nazov { get; set; }

	public List<Interpret> Interpreti { get; set; } = new();
	public List<Vydavatelstvo> Vydavatelstva { get; set; } = new();
}
