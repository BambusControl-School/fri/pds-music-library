﻿using System.Diagnostics.CodeAnalysis;

namespace DataConnector.Data;

[SuppressMessage("ReSharper", "ClassNeverInstantiated.Global")]
[SuppressMessage("ReSharper", "MemberCanBeInternal")]
[SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Global")]
[SuppressMessage("ReSharper", "CollectionNeverUpdated.Global")]
public class Skladba
{
	public int Id { get; set; }
	public string? Nazov { get; set; }
	public uint? PoradoveCislo { get; set; }
	public TimeSpan? Dlzka { get; set; }
	public DateTime? Vydane { get; set; }
	public bool JeOblubena { get; set; } = false;

	public int? IdAlbum { get; set; }
	public Album? Album { get; set; }

	public int? IdZaner { get; set; }
	public Zaner? Zaner { get; set; }

	public List<Interpret> Interpreti { get; set; } = new();
	public List<Playlist> Playlisty { get; set; } = new();
}
