﻿using System.Diagnostics.CodeAnalysis;

namespace DataConnector.Data;

[SuppressMessage("ReSharper", "ClassNeverInstantiated.Global")]
[SuppressMessage("ReSharper", "MemberCanBeInternal")]
[SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Global")]
[SuppressMessage("ReSharper", "CollectionNeverUpdated.Global")]
public class Playlist
{
	public int Id { get; set; }
	public string? Nazov { get; set; }
	public DateTime? Vytvorene { get; set; }

	public List<Skladba> Skladby { get; set; } = new();
}
