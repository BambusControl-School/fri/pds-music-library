﻿using System.Diagnostics.CodeAnalysis;

namespace DataConnector.Data;

[SuppressMessage("ReSharper", "ClassNeverInstantiated.Global")]
[SuppressMessage("ReSharper", "MemberCanBeInternal")]
[SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Global")]
[SuppressMessage("ReSharper", "CollectionNeverUpdated.Global")]
public class Vydavatelstvo
{
	public int Id { get; set; }
	public string? Nazov { get; set; }
	public DateTime? Od { get; set; }
	public DateTime? Do { get; set; }

	public int? IdKrajina { get; set; }
	public Krajina? Krajina { get; set; }

	public List<Album> Albumy { get; set; } = new();
}
