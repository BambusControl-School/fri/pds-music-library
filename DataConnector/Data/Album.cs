﻿using System.Diagnostics.CodeAnalysis;

namespace DataConnector.Data;

[SuppressMessage("ReSharper", "ClassNeverInstantiated.Global")]
[SuppressMessage("ReSharper", "MemberCanBeInternal")]
[SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Global")]
[SuppressMessage("ReSharper", "CollectionNeverUpdated.Global")]
public class Album
{
	public int Id { get; set; }
	public string? Nazov { get; set; }
	public DateTime? Vydane { get; set; }

	public int? IdHlavnyInterpret { get; set; }
	public Interpret? HlavnyInterpret { get; set; }

	public int? IdVydavatel { get; set; }
	public Vydavatelstvo? Vydavatel { get; set; }

	public int? IdZaner { get; set; }
	public Zaner? Zaner { get; set; }

	public List<Skladba> Skladby { get; set; } = new();
}
