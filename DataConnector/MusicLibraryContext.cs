﻿using DataConnector.Data;
using Microsoft.EntityFrameworkCore;
using Oracle.ManagedDataAccess.Client;

namespace DataConnector;

public class MusicLibraryContext : DbContext
{
	// ReSharper disable NullableWarningSuppressionIsUsed
	public DbSet<Zaner> Zanre { get; set; } = null!;
	public DbSet<Krajina> Krajiny { get; set; } = null!;
	public DbSet<Interpret> Interpreti { get; set; } = null!;
	public DbSet<Vydavatelstvo> Vydavatelstva { get; set; } = null!;
	public DbSet<Album> Albumy { get; set; } = null!;
	public DbSet<Skladba> Skladby { get; set; } = null!;
	public DbSet<RelationInterpretSkladby> RelationInterpretiSkladby { get; set; } = null!;
	public DbSet<RelationPlaylistSkladby> RelationSkladbyPlaylistu { get; set; } = null!;
	// ReSharper restore NullableWarningSuppressionIsUsed

	protected override void OnModelCreating(ModelBuilder modelBuilder)
	{
		modelBuilder.Entity<Zaner>().Define();
		modelBuilder.Entity<Krajina>().Define();
		modelBuilder.Entity<RelationInterpretSkladby>().Define();
		modelBuilder.Entity<Interpret>().Define();
		modelBuilder.Entity<Vydavatelstvo>().Define();
		modelBuilder.Entity<Album>().Define();
		modelBuilder.Entity<Skladba>().Define();
		modelBuilder.Entity<RelationPlaylistSkladby>().Define();
		modelBuilder.Entity<Playlist>().Define();

		base.OnModelCreating(modelBuilder);
	}

	protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
	{
		var dbConnection = new OracleConnectionStringBuilder
		{
			UserID = "majer15",
			Password = "mm561586",
			DataSource = "(DESCRIPTION=(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=obelix.fri.uniza.sk)(PORT=1521)))(CONNECT_DATA=(SERVER=DEDICATED)(SID=orcl)))"
		};

		optionsBuilder
			.UseOracle(dbConnection.ConnectionString)
			.UseUpperSnakeCaseNamingConvention()
			.EnableDetailedErrors()
			.EnableSensitiveDataLogging()
			// .LogTo(Console.Error.WriteLine)
			;

		base.OnConfiguring(optionsBuilder);
	}
}
