﻿using Microsoft.EntityFrameworkCore;

namespace MojoExtensions.Extensions;

public static class MyLinqExtensions
{
    public static async Task<ILookup<TKey, TSource>> ToLookupAsync<TKey, TSource>(
        this IQueryable<TSource> source,
        Func<TSource, TKey> keySelector,
        CancellationToken cancellationToken = default
    )
    {
        return (await source.ToListAsync(cancellationToken)).ToLookup(keySelector);
    }

    public static async Task<ILookup<TKey, TElement>> ToLookupAsync<TKey, TSource, TElement>(
        this IQueryable<TSource> source,
        Func<TSource, TKey> keySelector,
        Func<TSource, TElement> elementSelector,
        CancellationToken cancellationToken = default
    )
    {
        return (await source.ToListAsync(cancellationToken)).ToLookup(keySelector, elementSelector);
    }
}
