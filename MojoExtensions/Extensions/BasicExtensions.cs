﻿using System.Diagnostics.CodeAnalysis;

namespace MojoExtensions.Extensions;

public static class BasicExtensions
{
    public static string Format(this string formatString, object arg0)
        => string.Format(formatString, arg0);

    public static string Format(this string formatString, object arg0, object arg1)
        => string.Format(formatString, arg0, arg1);

    public static string Format(this string formatString, object arg0, object arg1, object arg2)
        => string.Format(formatString, arg0, arg1, arg2);

    public static string Format(this string formatString, params object[] args)
        => string.Format(formatString, args);

    public static bool IsNullOrEmpty([NotNullWhen(false)] this string? str)
        => string.IsNullOrEmpty(str);

    public static bool IsNullOrWhitespace([NotNullWhen(false)] this string? str)
        => string.IsNullOrWhiteSpace(str);

    public static string Join(this char separator, IEnumerable<string> values)
        => string.Join(separator, values);

    public static string Join(this string separator, IEnumerable<string> values)
        => string.Join(separator, values);

    public static string ExpandToString(this IEnumerable<string> values, char separator = ',')
        => $"[{separator.Join(values)}]";

    public static Task<IEnumerable<T>> AsEnumerableTask<T>(this Task<IEnumerable<T>> task)
        => task.ContinueWith(t => t.Result.AsEnumerable());

    public static Task<IEnumerable<T>> AsEnumerableTask<T>(this Task<T[]> task)
        => task.ContinueWith(t => t.Result.AsEnumerable());

    public static async Task<IEnumerable<TOut>> SelectAsync<TIn, TOut>(this Task<IEnumerable<TIn>> source, Func<TIn, TOut> selector)
        => (await source).Select(selector);
}
