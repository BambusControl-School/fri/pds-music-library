﻿using DataConnector;
using Microsoft.EntityFrameworkCore;

Console.WriteLine("Start!");

await using var db = new MusicLibraryContext();

// Vyber skladby britských interpretov, ktoré majú dĺžku najviac 4 minúty
var meQuery =
	from skladba in db.Skladby
	join relation in db.RelationInterpretiSkladby on skladba.Id equals relation.IdSkladba
	join interpret in db.Interpreti on relation.IdInterpret equals interpret.Id
	where interpret.Krajina.Nazov == "United Kingdom" && skladba.Dlzka < TimeSpan.FromMinutes(4)
	select $"{skladba.Nazov} [{skladba.Dlzka:g}] : ({interpret.Nazov})";

var queryResult = await meQuery.ToListAsync();

Console.WriteLine("---------------------\n" + string.Join('\n', queryResult) + "\n---------------------");

Console.WriteLine("End!");
