﻿using System;
using System.Linq;
using System.Windows.Controls;
using DataConnector;
using Microsoft.EntityFrameworkCore;
using PDS_Music_Library_Test.MVVM.Model;

namespace PDS_Music_Library_Test.MVVM.View;

public partial class SkladbyView : UserControl
{
	private const string NotApplicable = "N/A";

	public SkladbyView()
	{
		InitializeComponent();

		using var db = new MusicLibraryContext();


		var data = db.Skladby
			.Include(s => s.Album)
			.Include(s => s.Interpreti)
			.Include(s => s.Zaner)
			.AsEnumerable()
			.Select(item => new Skladba(
				item.Nazov ?? NotApplicable,
				item.JeOblubena,
				item.Dlzka ?? TimeSpan.Zero,
				item.Album?.Nazov ?? NotApplicable,
				item.Interpreti.FirstOrDefault()?.Nazov ?? NotApplicable,
				item.Zaner?.Nazov ?? NotApplicable
			));

		foreach (var item in data)
		{
			DataGridXaml.Items.Add(item);
		}
	}
}
