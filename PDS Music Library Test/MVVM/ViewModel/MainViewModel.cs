﻿using System.ComponentModel;
using PDS_Music_Library_Test.Core;

namespace PDS_Music_Library_Test.MVVM.ViewModel;

public class MainViewModel : NotifyPropertyChangedBase
{
	private object? _currentView;

	public BasicCommand ViewInterpreti { get; }
	public BasicCommand ViewAlbumy { get; }
	public BasicCommand ViewSkladby { get; }

	public MainViewModel()
	{
		ViewInterpreti = new BasicCommand(_ => CurrentView = new InterpretiViewModel());
		ViewAlbumy = new BasicCommand(_ => CurrentView = new AlbumyViewModel());
		ViewSkladby = new BasicCommand(_ => CurrentView = new SkladbyViewModel());

		CurrentView = ViewSkladby;
	}

	public object? CurrentView
	{
		get => _currentView;

		private set
		{
			if (!Equals(value, _currentView))
			{
				_currentView = value;
				OnPropertyChanged();
			}
		}
	}
}
