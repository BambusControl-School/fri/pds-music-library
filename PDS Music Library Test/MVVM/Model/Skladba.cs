﻿using System;

namespace PDS_Music_Library_Test.MVVM.Model;

internal record Skladba
(
	string Nazov,
	bool JeOblubena,
	TimeSpan Dlzka,
	string Album,
	string Interpret,
	string Zaner
);
