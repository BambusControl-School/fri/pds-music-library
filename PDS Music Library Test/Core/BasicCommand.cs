﻿using System;
using System.Windows.Input;

namespace PDS_Music_Library_Test.Core;

public sealed class BasicCommand : ICommand
{
	private readonly Action<object?> _execute;
	private readonly Func<object?, bool>? _canExecute;

	public event EventHandler? CanExecuteChanged
	{
		add => CommandManager.RequerySuggested += value;
		remove => CommandManager.RequerySuggested -= value;
	}

	public BasicCommand(Action<object?> execute, Func<object?, bool>? canExecute = null)
	{
		_execute = execute;
		_canExecute = canExecute;
	}

	public bool CanExecute(object? parameter) => _canExecute == null || _canExecute(parameter);
	public void Execute(object? parameter) => _execute(parameter);
}
